﻿using System;
using System.IO;

namespace Bigrams
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileLocation = "";

            // Get file path from args if provided or prompt user in console if not.
            if (args.Length == 0)
            {
                Console.WriteLine("Please provide the location of the .txt file to read for bigram analysis:");
                fileLocation = Console.ReadLine();
            }
            else
            {
                fileLocation = args[0];
            }

            string textToParse = "";

            // Read the file or exit on failure.
            try
            {
                textToParse = String.Join(" ", File.ReadAllLines(fileLocation));
            }
            catch (Exception ex)
            {
                PrintErrorAndExit($"Could not read the file provided: {ex.Message}");
            }

            var cleanedWords = WordParser.GetWordsClean(textToParse);

            // Check for null or empty word list.
            if (cleanedWords == null || cleanedWords.Length == 0)
            {
                PrintErrorAndExit("No words were found for the bigram analysis.");
            }

            BigramAnalyser bigramAnalyser = new BigramAnalyser(cleanedWords);
            bigramAnalyser.CountBigrams();
            bigramAnalyser.PrintBigrams();

            Console.Read();
        }

        static void PrintErrorAndExit(string message)
        {
            Console.WriteLine(message);
            Console.WriteLine("Press any key to exit.");
            Console.Read();
            Environment.Exit(0);
        }
    }

}
