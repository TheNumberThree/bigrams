﻿using System.Text;
using System.Linq;

namespace Bigrams
{
    public static class WordParser
    {
        public static string[] GetWordsClean(string input)
        {
            // Keep only letters and single whitespaces. Remove capitalization and chain whitespaces.
            var stringBuilder = new StringBuilder();

            foreach (var letter in input)
            {
                if (char.IsLetter(letter) || char.IsWhiteSpace(letter))
                {
                    stringBuilder.Append(letter);
                }
            }

            return stringBuilder.ToString()
                .ToLower()
                .Trim()
                .Split(' ')
                .Where(c => !string.IsNullOrWhiteSpace(c))
                .ToArray();
        }
    }
}