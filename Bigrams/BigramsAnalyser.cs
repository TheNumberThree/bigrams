﻿using System;
using System.Collections.Generic;

namespace Bigrams
{
    public class BigramAnalyser
    {
        string[] Bigrams { get; set; }
        public Dictionary<string, int> BigramsDictionary { get; set; }

        public BigramAnalyser(string[] inputs)
        {
            Bigrams = inputs;
            BigramsDictionary = new Dictionary<string, int>();
        }

        public void CountBigrams()
        {
            for (int i = 0; i < Bigrams.Length - 1; i++)
            {
                string currentBigram = $"{Bigrams[i]} {Bigrams[i + 1]}";
                if (BigramsDictionary.ContainsKey(currentBigram))
                {
                    BigramsDictionary[currentBigram]++;
                }
                else
                {
                    BigramsDictionary.Add(currentBigram, 1);
                }
            }
        }

        public void PrintBigrams()
        {
            foreach (var bigram in BigramsDictionary)
            {
                Console.WriteLine($"\"{bigram.Key}\" {bigram.Value}");
            }
        }
    }
}