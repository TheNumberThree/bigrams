﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bigrams;
using System.Collections.Generic;
using System;

namespace BigramsTest
{
    [TestClass]
    public class WordParserTest
    {
        [TestMethod]
        public void ParserTrimsAndRemovesSpecialCharacters()
        {
            string[] expectedArray = new string[] { "a", "string", "with", "strange", "characters" };

            string[] parserOutput = WordParser.GetWordsClean("    a str1ing w!#.,!?ith     stra@nge3 char^&*acters)({.   ");

            CollectionAssert.AreEqual(expectedArray, parserOutput);
        }

        [TestMethod]
        public void ParserReadsMultiLineInput()
        {
            string[] expectedArray = new string[] { "a", "string", "with", "strange", "characters" };

            var multiLineInput = new string[] { "a string", "with strange characters" };
            string[] parserOutput = WordParser.GetWordsClean(String.Join(" ", multiLineInput));

            CollectionAssert.AreEqual(expectedArray, parserOutput);
        }
    }
}
