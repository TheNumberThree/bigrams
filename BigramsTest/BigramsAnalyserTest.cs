﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bigrams;
using System.Collections.Generic;

namespace BigramsTest
{
    [TestClass]
    public class BigramsAnalyserTest
    {
        [TestMethod]
        public void BigramItemHasCountOfTwo()
        {
            // Arrange            
            int expectedInt = 2;
            string[] inputs = "the quick brown fox and the quick blue hare".Split(' ');
            BigramAnalyser analyser = new BigramAnalyser(inputs);

            // Act
            analyser.CountBigrams();

            // Assert
            Assert.AreEqual(expectedInt, analyser.BigramsDictionary["the quick"]);
        }

        [TestMethod]
        public void BigramHasCountsOfTwoAndThree()
        {
            int expectedFirstCount = 3;
            int expectedSecondCount = 2;

            string[] inputs = "the quick brown fox and the quick brown hare and the quick jumping spider".Split(' ');
            BigramAnalyser analyser = new BigramAnalyser(inputs);

            analyser.CountBigrams();

            Assert.AreEqual(expectedFirstCount, analyser.BigramsDictionary["the quick"]);
            Assert.AreEqual(expectedSecondCount, analyser.BigramsDictionary["quick brown"]);
        }       

        [TestMethod]
        public void TestBigramDictionaryCount()
        {
            Dictionary<string, int> expectedDictionary = new Dictionary<string, int>();
            expectedDictionary.Add("the quick", 1);
            expectedDictionary.Add("quick brown", 1);
            expectedDictionary.Add("brown fox", 1);
            expectedDictionary.Add("fox jumps", 1);
            expectedDictionary.Add("jumps over", 1);
            expectedDictionary.Add("over the", 1);
            expectedDictionary.Add("the lazy", 1);
            expectedDictionary.Add("lazy dog", 1);

            BigramAnalyser bigramAnalyser = new BigramAnalyser("the quick brown fox jumps over the lazy dog".Split(' '));
            bigramAnalyser.CountBigrams();

            CollectionAssert.AreEqual(expectedDictionary, bigramAnalyser.BigramsDictionary);
        }
    }
}
